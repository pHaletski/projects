﻿namespace Common.Interfaces
{
    public interface ILogger
    {
        void Info(object message);

        void Debug(object message);

        void Error(object message);
    }
}
