﻿using Common.Interfaces;
using log4net;
using log4net.Config;
using System;

namespace Common
{
    public class Logger : ILogger
    {
        private static readonly Lazy<ILog> _logger = new Lazy<ILog>(() => {

            XmlConfigurator.Configure();

            return LogManager.GetLogger("LOGGER");

        });

        private static ILog _loggerValue => _logger.Value;

        public void Debug(object message)
        {
            _loggerValue.Debug(message);
        }

        public void Error(object message)
        {
            _loggerValue.Error(message);
        }

        public void Info(object message)
        {
            _loggerValue.Info(message);
        }
    }
}
