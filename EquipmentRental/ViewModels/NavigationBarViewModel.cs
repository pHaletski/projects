﻿using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace EquipmentRental.ViewModels
{
    public class NavigationBarViewModel
    {
        public List<MenuItem> MenuItems { get; set; }

        public int BasketItemsCount { get; set; }
    }
}