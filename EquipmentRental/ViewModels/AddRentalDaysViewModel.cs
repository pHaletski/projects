﻿using System.ComponentModel.DataAnnotations;

namespace EquipmentRental.ViewModels
{
    public class AddRentalDaysViewModel
    {
        [Display(Name = "Rental days")]
        [Required(ErrorMessage = "Rental Days field is required")]
        [Range(1, 365, ErrorMessage = "Rental Days range should be between 1 and 365")]
        public int? RentalDays { get; set; }

        public string EquipmentId { get; set; }
    }
}