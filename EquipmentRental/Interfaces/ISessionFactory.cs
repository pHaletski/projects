﻿namespace EquipmentRental.Interfaces
{
    public interface ISessionFactory
    {
        T GetItem<T>(string key) where T : new();

        void Clear();
    }
}
