﻿using AutoMapper;
using EquipmentRental.BusinessLogic.Dtos;
using EquipmentRental.BusinessLogic.Interfaces;
using EquipmentRental.Constants;
using EquipmentRental.Interfaces;
using EquipmentRental.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EquipmentRental.Controllers
{
    public class BasketController : Controller
    {
        private readonly BasketViewModel _basket;
        private readonly IMapper _mapper;
        private readonly ISessionFactory _sessionFactory;
        private readonly IRentalService _rentalService;

        public BasketController(IMapper mapper,
            ISessionFactory sessionFactory,
            IRentalService rentalService)
        {
            _mapper = mapper;
            _sessionFactory = sessionFactory;
            _rentalService = rentalService;
            _basket = sessionFactory.GetItem<BasketViewModel>("Basket");
        }

        public ActionResult AddRentalDays(string equipmentId)
        {
            if (string.IsNullOrEmpty(equipmentId))
            {
                throw new ArgumentNullException(nameof(equipmentId));
            }

            var model = new AddRentalDaysViewModel
            {
                EquipmentId = equipmentId
            };

            return PartialView("_AddRentalDays", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddRentalDays(AddRentalDaysViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("_AddRentalDays", model);
            }

            await _basket.AddAsync(model.EquipmentId, model.RentalDays.GetValueOrDefault());

            return Json(new
            {
                isSuccess = true,
                redirectUrl = Url?.Action("Basket", "Basket")
            });
        }

        public async Task<ActionResult> Checkout()
        {
            var rentalDetails = _mapper.Map<List<BasketItem>, IEnumerable<RentalDetailDto>>(_basket.BasketItems);

            await _rentalService.CreateAsync(rentalDetails, FakeUserInfo.Email);

            _sessionFactory.Clear();

            return View("Confirmation");
        }

        public ActionResult Basket()
        {
            return PartialView("_Basket", _basket);
        }
    }
}