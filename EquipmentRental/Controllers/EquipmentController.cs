﻿using AutoMapper;
using EquipmentRental.BusinessLogic.Dtos;
using EquipmentRental.BusinessLogic.Interfaces;
using EquipmentRental.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EquipmentRental.Controllers
{
    public class EquipmentController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IEquipmentService _equipmentService;

        public EquipmentController(IMapper mapper, IEquipmentService equipmentService)
        {
            _mapper = mapper;
            _equipmentService = equipmentService;
        }

        public async Task<ActionResult> Index()
        {
            var equipmentsDto = await _equipmentService.GetAllAsync();
            var equipments = _mapper.Map<IEnumerable<EquipmentDto>, IEnumerable<Equipment>>(equipmentsDto);

            return View(equipments);
        }
    }
}