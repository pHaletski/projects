var equipmentrental = window.equipmentrental || {};
equipmentrental.basket = equipmentrental.basket || {};

equipmentrental.basket = (function () {
    var modalClasses = ["modal-sm", "modal-lg"];

    function init() {
        $(document).on('click', '.basket-button', function (event) {
            event.preventDefault();
            
            var selector = $(this).data('selector');
            var modal = $(this).data('target');
            var modalDialog = $(modal).find('.modal-dialog');

            $(modalDialog).removeClass(modalClasses)
                .addClass($(this).data("modal-type"));

            $.get($(this).data('href'), function (response) {
                $(selector).html(response);
                var count = $(response).data("basket-items-count");

                $('#shoppingBasketItems').text(count);
            });
        });
    }

    return {
        init: init
    };

})();