﻿using AutoMapper;
using EquipmentRental.BusinessLogic.Utilities;
using EquipmentRental.Utilities;

namespace EquipmentRental.App_Start
{
    public class AutoMapperConfig
    {
        public static IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg => 
            {
                cfg.AddProfile<AutoMapperWebProfile>();
                cfg.AddProfile<BusinessLogicAutoMapperProfile>();
            });

            return config.CreateMapper();
        }
    }
}