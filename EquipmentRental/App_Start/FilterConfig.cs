﻿using EquipmentRental.Attributes;
using System.Web.Mvc;

namespace EquipmentRental
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new ExceptionFilterAttribute());
        }
    }
}
