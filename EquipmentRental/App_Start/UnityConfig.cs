using EquipmentRental.App_Start;
using EquipmentRental.BusinessLogic.Utilities;
using EquipmentRental.Interfaces;
using EquipmentRental.Utilities;
using System;
using Unity;

namespace EquipmentRental
{
    public static class UnityConfig
    {
        #region Unity Container
        private static readonly Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

        public static IUnityContainer Container => container.Value;
        #endregion

        public static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterInstance(AutoMapperConfig.GetMapper());
            container.RegisterType<ISessionFactory, SessionFactory>();
            container.AddNewExtension<CommonDependencyInjection>();
            container.AddNewExtension<BusinessLogicDependencyInjection>();
        }
    }
}