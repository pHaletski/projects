﻿using EquipmentRental.DataLayer.Interfaces;
using System.Data.Entity.Infrastructure;

namespace EquipmentRental.DataLayer
{
    public class ContextFactory : IDbContextFactory<Context>, IContextFactory
    {
        public Context Create()
        {
            return new Context("EquipmentRentalDb");
        }
    }
}
