﻿using System.Collections;
using System.Collections.Generic;

namespace EquipmentRental.DataLayer.Entities
{
    public class User : BaseEntity
    {
        public string Name { get; set; }

        // TODO: add hashed password
        // public string Password { get; set; }

        public string Email { get; set; }

        public ICollection<Rental> Rentals { get; set; } = new HashSet<Rental>();
    }
}
