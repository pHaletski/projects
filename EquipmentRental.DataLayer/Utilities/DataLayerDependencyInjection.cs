﻿using EquipmentRental.DataLayer.Interfaces;
using Unity;
using Unity.Extension;

namespace EquipmentRental.DataLayer
{
    public class DataLayerDependencyInjection : UnityContainerExtension
    {
        protected override void Initialize()
        {
            Container.RegisterType<IContextFactory, ContextFactory>();
        }
    }
}
