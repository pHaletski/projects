namespace EquipmentRental.DataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Equipments",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                        ImgSource = c.String(),
                        EquipmentType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RentalDetails",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        RentalId = c.String(maxLength: 128),
                        EquipmentId = c.String(maxLength: 128),
                        RentalDays = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Equipments", t => t.EquipmentId)
                .ForeignKey("dbo.Rentals", t => t.RentalId)
                .Index(t => t.RentalId)
                .Index(t => t.EquipmentId);
            
            CreateTable(
                "dbo.Rentals",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        StartDate = c.DateTime(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Rentals", "UserId", "dbo.Users");
            DropForeignKey("dbo.RentalDetails", "RentalId", "dbo.Rentals");
            DropForeignKey("dbo.RentalDetails", "EquipmentId", "dbo.Equipments");
            DropIndex("dbo.Rentals", new[] { "UserId" });
            DropIndex("dbo.RentalDetails", new[] { "EquipmentId" });
            DropIndex("dbo.RentalDetails", new[] { "RentalId" });
            DropTable("dbo.Users");
            DropTable("dbo.Rentals");
            DropTable("dbo.RentalDetails");
            DropTable("dbo.Equipments");
        }
    }
}
