﻿namespace EquipmentRental.DataLayer.Interfaces
{
    public interface IContextFactory
    {
        Context Create();
    }
}
