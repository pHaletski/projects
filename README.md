# Rental Equipments for Bondora

Before start:
- you should have SQL server

Before you launch application you should:
- clean solution
- restore nuget packages
- rebuild solution

According to this task I used MVC application with out API, in spite of in the task was written that we should use two tier applciations.
I did it because for it does not make any sense to use WEB API and MVC application.
During development I used following stack of technologies:
- Entity Framework
- ASP.NET MVC 5
- C#
- HTML
- CSS
- Bootstrap 4
- SASS
- NodeJs
- Gulp (pluging of node js to compile and minify SASS to CSS)
- Jquery

Deployed application url is: http://haletskaya-001-site2.etempurl.com
Hosting provider is free thats why I could load long time, do not nervous about it.

Hope it makes sense
