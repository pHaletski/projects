﻿using AutoMapper;
using EquipmentRental.BusinessLogic.Dtos;
using EquipmentRental.BusinessLogic.Interfaces;
using EquipmentRental.DataLayer.Entities;
using EquipmentRental.DataLayer.Interfaces;

namespace EquipmentRental.BusinessLogic.Services
{
    public class EquipmentService : BaseService<EquipmentDto, Equipment>, IEquipmentService
    {
        public EquipmentService(IContextFactory contextFactory, IMapper mapper) 
            : base(contextFactory, mapper)
        {
        }
    }
}
