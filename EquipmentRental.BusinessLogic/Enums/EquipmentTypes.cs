﻿namespace EquipmentRental.BusinessLogic.Enums
{
    public enum EquipmentTypes
    {
        Heavy,
        Regular,
        Specialized
    }
}
