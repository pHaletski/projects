﻿namespace EquipmentRental.BusinessLogic.Models
{
    public sealed class SpecializedPrice : Price
    {
        public SpecializedPrice(int rentalDays)
            : base(rentalDays)
        {
            _rentalDays = rentalDays;
        }

        public override decimal CalculateTotal()
        {
            return CalculateExtrasPrice(_rentalDays, 3);
        }
    }
}