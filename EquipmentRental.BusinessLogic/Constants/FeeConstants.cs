﻿namespace EquipmentRental.BusinessLogic.Constants
{
    public class FeeConstants
    {
        public const decimal OneTimeFee = 100;

        public const decimal PremiumFee = 60;

        public const decimal RegularFee = 40;
    }
}