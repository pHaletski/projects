﻿using EquipmentRental.BusinessLogic.Models;

namespace EquipmentRental.BusinessLogic.Dtos
{
    public class RentalDetailDto : BaseDto
    {
        public string RentalId { get; set; }

        public string EquipmentId { get; set; }

        public int RentalDays { get; set; }

        public decimal Price { get; set; }

        public virtual RentalDto Rental { get; set; }

        public virtual EquipmentDto Equipment { get; set; }
    }
}
