﻿using System;
using System.Collections.Generic;

namespace EquipmentRental.BusinessLogic.Dtos
{
    public class RentalDto : BaseDto
    {
        public DateTime StartDate { get; set; }

        public virtual ICollection<RentalDetailDto> RentalDetails { get; set; } = new HashSet<RentalDetailDto>();
    }
}
