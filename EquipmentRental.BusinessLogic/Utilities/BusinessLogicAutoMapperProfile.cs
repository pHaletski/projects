﻿using AutoMapper;
using EquipmentRental.BusinessLogic.Dtos;
using EquipmentRental.DataLayer.Entities;

namespace EquipmentRental.BusinessLogic.Utilities
{
    public class BusinessLogicAutoMapperProfile : Profile
    {
        public BusinessLogicAutoMapperProfile()
        {
            CreateMap<BaseDto, BaseEntity>().ReverseMap();
            CreateMap<EquipmentDto, Equipment>().ReverseMap();
            CreateMap<RentalDto, Rental>().ReverseMap();
            CreateMap<RentalDetailDto, RentalDetail>().ReverseMap();
        }
    }
}
