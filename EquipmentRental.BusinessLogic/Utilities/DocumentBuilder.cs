﻿using EquipmentRental.BusinessLogic.Interfaces;
using EquipmentRental.BusinessLogic.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xceed.Words.NET;

namespace EquipmentRental.BusinessLogic.Utilities
{
    public class DocumentBuilder : IDocumentBuilder
    {
        public byte[] BuildRentalInvoice(RentalInvoice rentalInvoice)
        {
            using (var stream = new MemoryStream())
            {
                using (var doc = DocX.Create(stream))
                {
                    var paragraph = doc.InsertParagraph("Rental Invoice");
                    paragraph.Alignment = Alignment.center;
                    paragraph.FontSize(16);
                    doc.InsertParagraph();

                    var table = BuildTable(doc, rentalInvoice.InvoiceItems);

                    doc.InsertTable(table);
                    doc.InsertParagraph();
                    doc.InsertParagraph($"Total price: {rentalInvoice.TotalPrice}").Alignment = Alignment.right;
                    doc.InsertParagraph($"Total bonus points: {rentalInvoice.TotalBonusPoints}").Alignment = Alignment.right;

                    doc.Save();
                }

                return stream.ToArray();
            }
        }

        private Table BuildTable(DocX doc, List<List<string>> items, int columnCount = 6)
        {
            var table = doc.AddTable(items.Count, columnCount);

            for (int i = 0; i < table.Rows.Count; i++)
            {
                for (int j = 0; j < table.Rows[i].Cells.Count; j++)
                {
                    table.Rows[i].Cells[j].Paragraphs.FirstOrDefault()?.Append(items[i][j]);
                }
            }

            return table;
        }
    }
}
