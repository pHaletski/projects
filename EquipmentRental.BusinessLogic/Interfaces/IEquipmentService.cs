﻿using EquipmentRental.BusinessLogic.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EquipmentRental.BusinessLogic.Interfaces
{
    public interface IEquipmentService
    {
        Task<EquipmentDto> GetAsync(string id);

        Task<IEnumerable<EquipmentDto>> GetAllAsync();
    }
}
