﻿using AutoMapper;
using EquipmentRental.BusinessLogic.Dtos;
using EquipmentRental.BusinessLogic.Interfaces;
using EquipmentRental.BusinessLogic.Services;
using EquipmentRental.DataLayer;
using EquipmentRental.DataLayer.Entities;
using EquipmentRental.DataLayer.Enums;
using EquipmentRental.DataLayer.Interfaces;
using InMemoryDbSet;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EquipmentRental.BusinessLogic.Tests.Services
{

    [TestFixture]
    class RentalServiceTests
    {
        private IRentalService _rentalService;

        private Mock<IContextFactory> _contextFactoryMock;
        private Mock<IMapper> _mapperMock;
        private Mock<IDocumentBuilder> _documentBuilderMock;
        private Mock<Context> _contextMock;

        private InMemoryDbSet<User> _users;
        private InMemoryDbSet<Equipment> _equipments;
        private InMemoryDbSet<Rental> _rentals;

        [SetUp]
        public void SetUp()
        {
            _users = new InMemoryDbSet<User>
            {
                new User
                {
                    Email = "test@test.com"
                }
            };

            _equipments = new InMemoryDbSet<Equipment>
            {
                new Equipment
                {
                    Id = "TestEquipment1",
                    Name = "TestName1",
                    EquipmentType = EquipmentTypes.Heavy
                },
                new Equipment
                {
                    Id = "TestEquipment2",
                    Name = "TestName2",
                    EquipmentType = EquipmentTypes.Heavy
                }
            };

            _rentals = new InMemoryDbSet<Rental>();

            _contextFactoryMock = new Mock<IContextFactory>();
            _mapperMock = new Mock<IMapper>();
            _documentBuilderMock = new Mock<IDocumentBuilder>();
            _contextMock = new Mock<Context>("testConnectionString");

            _contextMock.Setup(x => x.Users).Returns(_users);
            _contextMock.Setup(x => x.Equipments).Returns(_equipments);
            _contextMock.Setup(x => x.Rentals).Returns(_rentals);

            _contextFactoryMock.Setup(x => x.Create()).Returns(_contextMock.Object);

            _rentalService = new RentalService(_contextFactoryMock.Object, _mapperMock.Object, _documentBuilderMock.Object);
        }

        [Test]
        public void CreateAsync_When_Email_Is_Null_Throws_ArgumentNullException()
        {
            // Assert
            Assert.ThrowsAsync<ArgumentNullException>(() => _rentalService.CreateAsync(Enumerable.Empty<RentalDetailDto>(), null));
        }

        [Test]
        public void CreateAsync_When_RentailDetailDtos_Are_Empty_Throws_ArgumentNullException()
        {
            // Assert
            Assert.ThrowsAsync<ArgumentNullException>(() => _rentalService.CreateAsync(Enumerable.Empty<RentalDetailDto>(), "test@test.com"));
        }

        [Test]
        public async Task CreateAsync_Calls_Create()
        {
            // Arrange
            var rentalDetailDtos = GetRentalDetailDtos();

            // Act
            await _rentalService.CreateAsync(rentalDetailDtos, "test@test.com");

            // Assert
            _contextFactoryMock.Verify(x => x.Create(), Times.Once);
        }

        [Test]
        public async Task CreateAsync_Adds_Rentals()
        {
            // Arrange
            var rentalDetailDtos = GetRentalDetailDtos();

            // Act
            var rentalId = await _rentalService.CreateAsync(rentalDetailDtos, "test@test.com");
            var result = _rentals.FirstOrDefault(x => x.Id == rentalId);

            // Assert
            Assert.IsNotNull(result);
        }

        [TestCase(120, Enums.EquipmentTypes.Specialized, 2, TestName = "Calculates_PremiumFee_For_First_ThreeDays")]
        [TestCase(260, Enums.EquipmentTypes.Specialized, 5, TestName = "Calculates_PremiumFee_For_First_ThreeDays_And_RegularFee_Over_ThreeDays")]
        [TestCase(220, Enums.EquipmentTypes.Regular, 2, TestName = "Calculates_OneTimeFee_Plus_PremiumFee_For_First_TwoDays")]
        [TestCase(340, Enums.EquipmentTypes.Regular, 5, TestName = "Calculates_OneTimeFee_Plus_PremiumFee_For_First_TwoDays_And_RegularFee_Over_TwoDays")]
        [TestCase(280, Enums.EquipmentTypes.Heavy, 3, TestName = "Calculates_OneTimeFee_Plus_PremiumFee_For_Each_Day")]
        public void GetPrice_Returns_Price(decimal expectedResult, Enums.EquipmentTypes equipmentType, int rentalDays)
        {
            // Act
            var result = _rentalService.GetPrice(equipmentType, rentalDays);

            // Assert
            Assert.AreEqual(expectedResult, result, $"Actual result is {result}, but expected was {expectedResult}");
        }

        private IEnumerable<RentalDetailDto> GetRentalDetailDtos()
        {
            return new List<RentalDetailDto>
            {
                new RentalDetailDto
                {
                    EquipmentId = "TestEquipment1",
                    RentalDays = 5,
                    RentalId = "TestRental1"
                },
                new RentalDetailDto
                {
                    EquipmentId = "TestEquipment1",
                    RentalDays = 10,
                    RentalId = "TestRental1"
                }
            };
        }
    }
}
